package com.gable.demo.doe;

import org.springframework.data.repository.CrudRepository;

import com.gable.demo.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
